CC     =g++
CFLAGS =-c -Wall -std=c++11 -g -D_DEBUG -Wno-format-extra-args -Wno-format -D_IPV6
LDFLAGS=-g
SOURCES=kasumi.cpp kasumiserver.cpp easysocket.cpp netns.cpp netnsmanager.cpp iface.cpp ifacemanager.cpp server.cpp
OBJECTS=$(SOURCES:.cpp=.o)
OUTPUT =kasumi

all: $(SOURCES) $(OUTPUT)

$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm *.o
	rm $(OUTPUT)
	rm .depends

depends: .depends
.depends: $(SOURCES)
	rm -f .depends
	@$(CC) $(CFLAGS) -MM $^>> .depends

-include .depends

