#include "easysocket.hpp"

EasySocketHandler::EasySocketHandler() {
	
}

EasySocketHandler::~EasySocketHandler() {
	this->closeAll();
}


EasySocket *EasySocketHandler::createTunnel(char *tunName, int flags, int *err) {
	struct ifreq ifr;
	
	*err = 0;
	
	EasySocket_ptr tun(new EasySocket());
	
	tun->handle = open("/dev/net/tun", O_RDWR);
	if (tun->handle == -1) {
		DEBUG("Error opening tunnel", 0);
		
		*err = errno;
		return nullptr;
	}
	
	ifr.ifr_flags = IFF_TUN | flags;
	if (*tunName) {
		strncpy(ifr.ifr_name, tunName, IFNAMSIZ);
	}
	
	*err = ioctl(tun->handle, TUNSETIFF, (void *)&ifr);
	if (*err == -1) {
		DEBUG("Error at ioctl", 0);
		*err = errno;
		
		::close(tun->handle);
		return nullptr;
	}
	
	strcpy(tun->name, ifr.ifr_name);
	
	this->file_descriptors.push_back(std::move(tun));
	
	*err = 0;
	return this->file_descriptors.back().get();
}

EasySocket *EasySocketHandler::createSocket(int domain, int type, int protocol, int *err) {
	*err = 0;
	
	EasySocket_ptr sock(new EasySocket());
	
	sock->handle = ::socket(domain, type, protocol);
	if (sock->handle == -1) {
		*err = errno;
		return nullptr;
	}
	
	int optval = 1;
	*err = setsockopt(sock->handle, SOL_SOCKET, SO_REUSEADDR, (char *)&optval, sizeof(optval));
	if (*err == -1) {
		this->close(sock.get());
		*err = errno;
		return nullptr;
	}
	
	this->file_descriptors.push_back(std::move(sock));
	
	return this->file_descriptors.back().get();
}

int EasySocketHandler::bind(EasySocket *sock, Socket_Addr *local) {
	int err = ::bind(sock->handle, (struct sockaddr *)local, sizeof(Socket_Addr));
	
	if (err == -1)
		return errno;
	
	return 0;
}

int EasySocketHandler::listen(EasySocket *sock, int bl) {
	int err = ::listen(sock->handle, bl);
	
	if (err == -1)
		return errno;
	
	return 0;
}

EasySocket *EasySocketHandler::accept(EasySocket *sock, Socket_Addr *remote, socklen_t *remote_len, int *err) {
	EasySocket_ptr incoming(new EasySocket());
	
	incoming->handle = ::accept(sock->handle, (struct sockaddr *)remote, remote_len);
	if (incoming->handle == -1) {
		if (errno == EAGAIN || errno == EWOULDBLOCK) {
			*err = errno;
			return nullptr;
		} else {
			*err = errno;
			return nullptr;
		}
	}
	
	this->file_descriptors.push_back(std::move(incoming));
	
	*err = 0;
	return this->file_descriptors.back().get();
}

int EasySocketHandler::connect(EasySocket *sock, Socket_Addr *remote) {
	int err = ::connect(sock->handle, (struct sockaddr *)remote, sizeof(Socket_Addr));
	
	if (err == -1)
		return errno;
	
	return 0;
}

int EasySocketHandler::close(EasySocket *sock) {
	int err = 0;
	err = ::close(sock->handle);
	
	if (err == -1)
		return errno;
	
	this->unregSocket(sock);
	
	return 0;
}

void EasySocketHandler::closeAll() {
	DEBUG("Closing all sockets", 0);
	for (uint16_t i = 0; i < this->file_descriptors.size(); i++) {
		EasySocket *s = this->file_descriptors[i].get();
		if (this->close(s)) {
			DEBUG("Error closing %ud", s->handle);
		}
	}
}

int EasySocketHandler::read(EasySocket *sock, char *buffer, int n, int *err) {
	int got = ::read(sock->handle, buffer, n);
	if (got == -1 && err != nullptr) {
		*err = got;
		return -1;
	}
	
	*err = 0;
	return got;
}

int EasySocketHandler::read_exactly(EasySocket *sock, char *buffer, int n, int *err) {
	int amt  = 0;
	int left = n;
	
	while (left > 0) {
		amt = this->read(sock, buffer, left, err);
		if (amt == 0)
			return 0;
		
		left   -= amt;
		buffer += amt;
	}
	
	return n;
}

int EasySocketHandler::write(EasySocket *sock, char *buffer, int n, int *err) {
	int got = ::write(sock->handle, buffer, n);
	if (got == -1 && err != nullptr) {
		*err = errno;
		return -1;
	}
	
	*err = 0;
	return got;
}

bool EasySocketHandler::readyToRead(EasySocket *sock) {
	if (sock == 0)
		return false;
	
	for (uint16_t i = 0; i < this->file_descriptors.size(); i++) {
		if (this->file_descriptors[i]->handle == sock->handle) {
			return (sock->r_events && POLLIN) ? true : false;
			//return (this->file_descriptors[i].revents && POLLIN) ? true : false;
		}
	}
	
	return false;
}

int EasySocketHandler::poll() {
	return this->poll(0);
}

int EasySocketHandler::poll(int ms) {
	std::vector<struct pollfd> fds;
	for (uint16_t i = 0; i < this->file_descriptors.size(); i++) {
		//DEBUG("%d", this->file_descriptors[i]->handle);
		
		fds.push_back(this->file_descriptors[i]->getPollFD());
	}
	
	
	// I love vectors
	int ret = ::poll(fds.data(), fds.size(), ms);
	
	for (uint16_t i = 0; i < fds.size(); i++) {
		this->file_descriptors[i]->r_events = fds[i].revents;
	}
	
	return ret;
}

bool EasySocketHandler::exists(EasySocket *sock) {
	for (uint16_t i = 0; i < this->file_descriptors.size(); i++) {
		if (this->file_descriptors[i]->handle == sock->handle) {
			return true;
		}
	}
	
	return false;
}

EasySocket *EasySocketHandler::regSocket(EasySocket *sock) {
	return nullptr;
}

void EasySocketHandler::unregSocket(EasySocket *sock) {
	for (uint16_t i = 0; i < this->file_descriptors.size(); i++) {
		if (this->file_descriptors[i]->handle == sock->handle) {
			this->file_descriptors.erase(this->file_descriptors.begin() + i);
			return;
		}
	}
}

struct pollfd EasySocket::getPollFD() {
	return (struct pollfd) { .fd = this->handle, .events = POLLIN, .revents = this->r_events };
}
