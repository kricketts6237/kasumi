#ifndef IFACE_HPP
#define IFACE_HPP

#include <vector>
#include <string>
#include <algorithm>
#include <net/if_arp.h>
#include <cerrno>
#include <cstring>

#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include "constants.hpp"

// The contents of this object are populated in ifacemanager.cpp
// This is to reduce the number of netlink calls required
// This applies for RTM_GETLINK, but IFace calls RTM_GETADDR
class IFace {
	public:
		typedef enum {
			LINK_UNKNOWN,
			
			LINK_NONE,
			LINK_VETH,
			LINK_BRIDGE,
			LINK_BOND,
			LINK_DUMMY,
			LINK_LOOPBACK,
			LINK_PHYSICAL,
			LINK_VLAN
		} LinkType;
	
	public:
		std::string name;
		int         type;
		LinkType    link_type;
		LinkType    slave_type;
		std::string link_type_name;
		std::string slave_type_name;
		int         master;
		int         peer;
		int         index;
		bool        up;
		
		
		std::vector<std::string> addresses;
		
		IFace();
		
		// Used to populate certain fields that RTNETLINK might not
		void typeCheck();
		
		static LinkType getLinkType(std::string lt);
		static LinkType getLinkType(char *lt);
};

#endif
