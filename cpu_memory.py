def get_ram_stats():
	ret = {}
	f = open("/proc/meminfo", "r")
	for line in f.readlines():
		triple = [x for x in line.split(" ") if x != '']
		ret[triple[0][:-1]] = int(triple[1])
	
	f.close()
	
	return ret

def get_network_in_out():
	ret = {}
	f = open("/proc/net/dev", "r")
	for line in f.readlines()[2:]:
		line = [x for x in line.strip().split(" ") if x != '']
		data = {}
		data['bytes_rx'] = int(line[1])
		data['packets_rx'] = int(line[2])
		data['error_rx'] = int(line[3])
		data['dropped_rx'] = int(line[4])
		
		data['bytes_tx'] = int(line[9])
		data['packets_tx'] = int(line[10])
		data['error_tx'] = int(line[11])
		data['dropped_tx'] = int(line[12])
		
		ret[line[0][:-1]] = data
	return ret

def get_cpu_temp():
	f = open("/sys/class/thermal/thermal_zone0/temp", "r")
	ret = int(f.read().strip())
	f.close()
	
	return ret

def get_cpu_loadavg():
	f = open("/proc/loadavg", "r")
	data = f.read().strip().split(" ")
	f.close()
	
	ret = {}
	ret['1'] = float(data[0])
	ret['5'] = float(data[1])
	ret['15'] = float(data[2])
	
	return ret

if __name__ == "__main__":
	main()
