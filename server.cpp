#include <iostream>
#include <signal.h>
#include <cstdio>
#include <vector>
#include <utility>
#include <iomanip>
#include <ifaddrs.h>

#include "easysocket.hpp"
#include "netnsmanager.hpp"
#include "ifacemanager.hpp"
#include "kasumi.hpp"

#ifdef __arm__
#define HOST_IFACE "tun0"
#else
#define HOST_IFACE "enp3s0"
#endif

// Font is called "lean"
std::string banner_text =                                                                
"    _/    _/                                                _/   \n"
"   _/  _/      _/_/_/    _/_/_/  _/    _/  _/_/_/  _/_/          \n"
"  _/_/      _/    _/  _/_/      _/    _/  _/    _/    _/  _/     \n"
" _/  _/    _/    _/      _/_/  _/    _/  _/    _/    _/  _/      \n"
"_/    _/    _/_/_/  _/_/_/      _/_/_/  _/    _/    _/  _/       \n";

bool done = false;
void sigint_handler(int a) {
	done = true;
}

int main(int argc, const char **argv) {
	std::string filename;
	
	if (logging_start("kasumi.log")) {
		std::cout << "Cannot start logging!" << std::endl;
		return 1;
	}
	
	log("INFO", "Starting kasumi\n");
	
	if (argc == 1) {
		filename = "hops_list.direct";
	} else {
		filename = argv[1];
	}
	
	std::cout << banner_text << std::endl;
	
	if (make_hop_map(filename)) {
		log("ERROR", "%s", "Unable to make hop map.\n");
		return 1;
	}
	
	//std::cout << "Hops" << std::endl;
	
	signal(SIGINT, sigint_handler);
	signal(SIGTERM, sigint_handler);
	
	EasySocketHandler handler;
	NetNSManager      ns_man;
	ns_man.populate();
	
	IP_Address host_ip;
	getIPForInterface(HOST_IFACE, &host_ip);
	
	char ip_addr_buff[64];
	#ifdef _IPV6
	inet_ntop(AF_INET6, &(host_ip), ip_addr_buff, 64);
	log("INFO", "Hosting on IPv6: %s\n", ip_addr_buff);
	#else
	inet_ntop(AF_INET, &(host_ip), ip_addr_buff, 64);
	log("INFO", "Hosting on IPv6: %s\n", ip_addr_buff);
	#endif
	
	char *buffer;
	char  relayName[IFNAMSIZ] = "relay\0";
	int   err;
	EasySocket *sock, *conn, *relay;
	
	std::vector<EasySocket *> clients;
	
	conn = 0;
	
	buffer = (char *)malloc(0x10000 + sizeof(KasumiHeader) + sizeof(IP_Address) * 256);
	
	#ifdef _IPV6
		sock = handler.createSocket(AF_INET6, SOCK_STREAM | O_NONBLOCK, 0, &err);
	#else
		sock = handler.createSocket(AF_INET, SOCK_STREAM | O_NONBLOCK, 0, &err);
	#endif
	
	NetNS *relay_ns = ns_man.getNamespaceByName("relay");
	if ((err = ns_man.enter(relay_ns))) {
		log("ERROR", "Unable to enter relay namespace: %s\n", strerror(err));
		return 1;
	}
	
	relay = handler.createTunnel(relayName, IFF_NO_PI, &err);
	
	if ((err = ns_man.leave())) {
		log("ERROR", "Unable to leave relay namespace: %s\n", strerror(err));
		return 1;
	}
	
	if (relay == nullptr || relay->handle == -1) {
		log("ERROR", "Unable to create relay: %s\n", strerror(err));
		free(buffer);
		return 1;
	}
	
	if (sock->handle == -1) {
		log("ERROR", "Unable to create socket: %s\n", strerror(err));
		free(buffer);
		return 1;
	}
	
	
	Socket_Addr local;
	memset(&local, 0, sizeof(local));
	
	#ifdef _IPV6
		local.sin6_family = AF_INET6;
		local.sin6_port   = htons(9000);
		local.sin6_addr   = host_ip;
	#else
		local.sin_family      = AF_INET;
		local.sin_port        = htons(9000);
		local.sin_addr.s_addr = htonl(INADDR_ANY);
	#endif
	
	err = handler.bind(sock, &local);
	if (err) {
		log("ERROR", "Unable to bind socket: %s\n", strerror(err));
		free(buffer);
		return 1;
	}
	
	err = handler.listen(sock, 5);
	if (err) {
		log("ERROR", "Unable to listen on socket: %s\n", strerror(err));
		free(buffer);
		return 1;
	}
	
	log("INFO", "Socket bound, listening, and ready.\n");
	
	Socket_Addr remote_addr;
	socklen_t   remote_addr_len;
	while (!done) {
		err = handler.poll(10);
		
		if (handler.readyToRead(relay)) {
			int got = handler.read(relay, buffer, 0x10000, &err);
			
			IP_Header *ip_hdr = (IP_Header *)buffer;
			char src_ip[64];
			char dst_ip[64];
			
			#ifdef _IPV6
				inet_ntop(AF_INET6, &(ip_hdr->source_ip), src_ip, 64);
				
				inet_ntop(AF_INET6, &(ip_hdr->dest_ip), dst_ip, 64);
			#else
				inet_ntop(AF_INET, &(ip_hdr->source_ip), src_ip, 64);
				
				inet_ntop(AF_INET, &(ip_hdr->dest_ip), dst_ip, 64);
			#endif
			
			#ifdef _IPV6
			if (ip_hdr->source_ip.s6_addr[0] == 0x00) {
			#else
			if (ip_hdr->source_ip.s_addr == 0x00000000) {
			#endif
				continue;
			}
			
			//std::cout << "Outbound Message" << std::endl;
			//std::cout << src_ip << " to " << dst_ip << std::endl;
			
			KasumiHeader kas_hdr;
			memcpy(kas_hdr.identifier, "KASUMIPROTOCOL01", 16);
			
			kas_hdr.command        = 0;
			kas_hdr.source         = ip_hdr->source_ip;
			kas_hdr.destination    = ip_hdr->dest_ip;
			kas_hdr.payload_length = got;
			kas_hdr.hops           = nullptr;
			
			std::vector<IP_Address> hops;
			
			err = getHops(kas_hdr.source, kas_hdr.destination, &hops);
			if (err == 1) {
				//std::cout << "Invalid destination" << std::endl;
				continue;
			} else if (err == 2) {
				//std::cout << "Invalid source" << std::endl;
				continue;
			}
			
			kas_hdr.hop_count = hops.size();
			
			log("INFO", "OUT %s -> %s  %d hops \n", src_ip, dst_ip, hops.size());
			//std::cout << "Hops: " << hops.size() << std::endl;
			for (uint16_t i = 0; i < hops.size(); i++) {
				char buff[64];
				
				//std::cout << "Hop through ";
				IP_Address addr;
				addr = hops.at(i);
				put_address(addr, buff);
				
				log("INFO", "HOP %s\n", buff);
				
				//std::cout << std::endl;
			}
			
			#ifdef _IPV6
			EasySocket *outbound = handler.createSocket(AF_INET6, SOCK_STREAM, 0, &err);
			#else
			EasySocket *outbound = handler.createSocket(AF_INET, SOCK_STREAM, 0, &err);
			#endif
			if (err) {
				//std::cout << "Could not open outbound socket!" << std::endl;
				//std::cout << strerror(err) << std::endl;
				
				log("ERROR", "Could not open outbound socket: %s\n", strerror(err));
				
				free(buffer);
				return 1;
			}
			
			IP_Address next_addr = kas_hdr.destination;
			if (hops.size() > 0) {
				next_addr = hops[0];
			}
			
			memset(&remote_addr, 0, sizeof(remote_addr));
			#ifdef _IPV6
			remote_addr.sin6_family = AF_INET6;
			remote_addr.sin6_addr   = next_addr;
			remote_addr.sin6_port   = htons(9000);
			#else
			remote_addr.sin_family      = AF_INET;
			remote_addr.sin_addr.s_addr = next_addr.s_addr;
			remote_addr.sin_port        = htons(9000);
			#endif
			
			
			err = handler.connect(outbound, &remote_addr);
			if (err) {
				//std::cout << "Could not connect outbound socket!" << std::endl;
				//std::cout << strerror(err) << std::endl;
				
				log("ERROR", "Could not connect outbound socket: %s\n", strerror(err));
				
				free(buffer);
				return 1;
			}
			
			int out = handler.write(outbound, (char *)&kas_hdr, sizeof(kas_hdr), &err);
			//std::cout << "Wrote " << out << " bytes" << std::endl;
			if (err) {
				//std::cout << "Write error (header): " << strerror(err) << std::endl;
				log("ERROR", "Write error (header): %s\n", strerror(err));
			}
			
			
			for (uint16_t i = 0; i < hops.size(); i++) {
				IP_Address addr;
				addr = hops.at(i);
				
				out = handler.write(outbound, (char *)&addr, sizeof(addr), &err);
				//std::cout << "Wrote " << out << " bytes" << std::endl;
				if (err) {
					//std::cout << "Write error (hops): " << strerror(err) << std::endl;
					log("ERROR", "Write error (hops): %s\n", strerror(err));
				}
			}
			
			
			out = handler.write(outbound, buffer, kas_hdr.payload_length, &err);
			//std::cout << "Wrote " << out << " bytes" << std::endl;
			if (err) {
				//std::cout << "Write error (payload): " << strerror(err) << std::endl;
				log("ERROR", "Write error (payload): %s\n", strerror(err));
			}
			
			handler.close(outbound);
			
			continue;
		}
		
		if (handler.readyToRead(sock)) {
			conn = handler.accept(sock, &remote_addr, &remote_addr_len, &err);
			//std::cout << "Something appeared on the socket" << std::endl;
			log("INFO", "Connection inbound\n");
			if (conn == nullptr) {
				if (err == EAGAIN || err == EWOULDBLOCK) {
					conn = 0;
					std::cout << "EWOULDBLOCK" << std::endl;
				} else {
					//std::cout << "Unable to accept socket " << strerror(err) << std::endl;
					log("ERROR", "Unable to accept socket: %s\n", strerror(err));
					free(buffer);
					return 1;
				}
			} else {
				if (err == 0) {
					log("INFO", "Connection accepted\n");
					clients.push_back(conn);
				}
			}
			
			continue;
		}
		
		if (handler.readyToRead(conn)) {
			KasumiHeader kas_hdr;
			
			memset(&kas_hdr, 0, sizeof(kas_hdr));
			
			log("INFO", "Message inbound\n");
			handler.read_exactly(conn, (char *)&kas_hdr, sizeof(KasumiHeader), &err);
			if (err) {
				//std::cout << "Error reading inbound kasumi header: " << strerror(err) << std::endl;
				log("ERROR", "Error reading inbound kasumi header: %s\n", strerror(err));
				break;
			}
			
			if (memcmp(&(kas_hdr.identifier), "KASUMIPROTOCOL01", 16)) {
				log("INFO", "Not a kasumi connection.\n");
				/*std::cout << "Not a kasumi connection" << std::endl;
				std::cout << "Got header identifier ";
				for (uint16_t i = 0; i < 16; i++) {
					std::cout << std::hex << (uint16_t)kas_hdr.identifier[i] << " ";
				}
				std::cout << std::endl;*/
				
				handler.close(conn);
				
				continue;
			}
			
			std::vector<IP_Address> hops;
			for (uint8_t i = 0; i < kas_hdr.hop_count; i++) {
				IP_Address hop;
				handler.read_exactly(conn, (char *)&hop, sizeof(IP_Address), &err);
				if (err) {
					//std::cout << "Error reading kasumi hop list: " << strerror(err) << std::endl;
					log("ERROR", "Error reading kasumi hop list: %s\n", strerror(err));
					break;
				} else {
					//std::cout << "HOP: ";
					//put_address(hop);
					//std::cout << std::endl;
					
					hops.push_back(hop);
				}
			}
			
			handler.read_exactly(conn, buffer, kas_hdr.payload_length, &err);
			if (err) {
				//std::cout << "Error reading inbound data: " << strerror(err) << std::endl;
				log("ERROR", "Error reading inbound data: %s\n", strerror(err));
				break;
			}
			
			handler.close(conn);
			
			//std::cout << "Read: " << kas_hdr.payload_length << " bytes" << std::endl;
			log("INFO", "Read %d bytes.\n", kas_hdr.payload_length);
			
			if (!memcmp((void *)&(kas_hdr.destination), (void *)&(host_ip), sizeof(IP_Address))) {
				//std::cout << "This packet is for us!" << std::endl;
				
				handler.write(relay, buffer, kas_hdr.payload_length, &err);
				if (err) {
					//std::cout << "Tunnel write error: " << strerror(err) << std::endl;
					log("ERROR", "Tunnel write error: %s\n", strerror(err));
				}
			} else {
				//std::cout << "This packet is NOT for us!" << std::endl;
				//std::cout << "DESTINATION: ";
				char buff[64];
				put_address(kas_hdr.destination, buff);
				std::cout << std::endl;
				
				#ifdef _IPV6
				EasySocket *outbound = handler.createSocket(AF_INET6, SOCK_STREAM, 0, &err);
				#else
				EasySocket *outbound = handler.createSocket(AF_INET, SOCK_STREAM, 0, &err);
				#endif
				if (err) {
					//std::cout << "Could not open outbound socket!" << std::endl;
					//std::cout << strerror(err) << std::endl;
					log("ERROR", "Could not open outbound socket: %s\n", strerror(err));
					
					free(buffer);
					return 1;
				}
				
				int hop_num = -1;
				for (uint8_t i = 0; i < hops.size(); i++) {
					if (!memcmp((void *)&(hops[i]), (void *)&(host_ip), sizeof(IP_Address))) {
						hop_num = i;
						break;
					}
				}
				
				IP_Address next_addr = kas_hdr.destination;
				if (hop_num > -1) {
					if ((unsigned int)(hop_num + 1) < hops.size())
						next_addr = hops[hop_num + 1];
				}
				
				memset(&remote_addr, 0, sizeof(remote_addr));
				
				
				#ifdef _IPV6
				remote_addr.sin6_family = AF_INET6;
				remote_addr.sin6_addr   = next_addr;
				remote_addr.sin6_port   = htons(9000);
				#else
				remote_addr.sin_family = AF_INET;
				remote_addr.sin_addr   = next_addr;
				remote_addr.sin_port   = htons(9000);
				#endif
				
				err = handler.connect(outbound, &remote_addr);
				if (err) {
					//std::cout << "Could not connect outbound socket!" << std::endl;
					//std::cout << strerror(err) << std::endl;
					log("ERROR", "Could not connect outbound socket: %s\n", strerror(err));
					
					free(buffer);
					return 1;
				}
				
				int out = handler.write(outbound, (char *)&kas_hdr, sizeof(kas_hdr), &err);
				//std::cout << "Wrote " << out << " bytes" << std::endl;
				if (err) {
					//std::cout << "Write error (header): " << strerror(err) << std::endl;
					log("ERROR", "Write error (header): %s\n", strerror(err));
				}
				
				
				for (uint16_t i = 0; i < hops.size(); i++) {
					IP_Address addr;
					addr = hops.at(i);
					
					out = handler.write(outbound, (char *)&addr, sizeof(addr), &err);
					//std::cout << "Wrote " << out << " bytes" << std::endl;
					if (err) {
						//std::cout << "Write error (hops): " << strerror(err) << std::endl;
						log("ERROR", "Write error (hops): %s\n", strerror(err));
					}
				}
				
				
				out = handler.write(outbound, buffer, kas_hdr.payload_length, &err);
				//std::cout << "Wrote " << out << " bytes" << std::endl;
				if (err) {
					//std::cout << "Write error (payload): " << strerror(err) << std::endl;
					log("ERROR", "Write error (payload): %s\n", strerror(err));
				}
				
				handler.close(outbound);
			}
		}
	}
	
	
	free(buffer);
	
	//std::cout << "Goodbye" << std::endl;
	log("INFO", "Kasumi closed normally.\n");
	
	logging_end();
	
	return 0;
}
