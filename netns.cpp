#include "netns.hpp"


NetNS::NetNS() {
	this->name   = "";
	this->path   = "";
	this->handle = 0;
}

int NetNS::prepare() {
	if (this->handle != 0)
		return EPERM;
	
	int fp = open(this->path.c_str(), O_RDONLY);
	if (fp == -1)
		return errno;
	
	this->handle = fp;
	
	return 0;
}

int NetNS::finish() {
	if (this->handle == 0)
		return EBADF;
	
	int err = close(this->handle);
	if (err == -1)
		return errno;
	
	this->handle = 0;
	
	return 0;
}

int NetNS::enter() {
	if (this->handle == 0)
		return EBADF;
	
	int err = setns(this->handle, 0);
	if (err == -1)
		return errno;
	
	return 0;
}
