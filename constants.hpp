#ifndef CONSTANTS_HPP
#define CONSTANTS_GPP

#define NETNS_PATH "/var/run/netns/"
#define NETNS_SELF "/proc/self/ns/net"

#define NL_RECV_LENGTH 16384

#endif
