import time
import socket
import signal
from sense_hat import SenseHat

sense = SenseHat()

done = False
def sigint_handler(num, frame):
	done = True

def main():
	sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	sock.bind(("fcc0:4301:5782:ab81:f533:5b07:c8f3:af89", 8192))
	#sock.listen(5)
	
	signal.signal(signal.SIGINT, sigint_handler)
	
	sense.clear([0, 0, 0])
	
	f = open("data_out.csv", "w")
	
	#col = [ram_stat['MemAvailable'], net_io['bytes_rx'], net_io['bytes_tx'], cpu_temp, cpu_load['1'], cpu_load['5'], cpu_load['15']]
	f.write(",".join([
		"Available RAM", "Bytes In", "Bytes Out", "CPU Temp", "Load (1 min)", "Load (5 min)", "Load (15 min)"
	]) + "\n")
	
	while True:
		#(client, addr) = sock.accept()
		
		pack, peer = sock.recvfrom(1024)
		#pack = client.recv(1024)
		#colors = [(int(x) if int(x) < 256 and int(x) >= 0 else 255) for x in pack.split(",")]
		#sense.clear(colors)

		#client.close()
		
		print "Got", peer + "," + pack
		
		f.write(pack + "\n")
		time.sleep(0.05)
	
	sock.shutdown()
	sock.close()
	f.close()

if __name__ == "__main__":
	main()
