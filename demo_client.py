import time
import socket
import signal
from random     import randint
from sense_hat  import SenseHat
from cpu_memory import *

sense = SenseHat()

done = False
def sigint_handler(num, frame):
	done = True

def main():

	signal.signal(signal.SIGINT, sigint_handler)

	while True:
		#sense.stick.wait_for_event()
		#col = [randint(0,255), randint(0,255), randint(0,255)]
		#col = [sense.get_humidity(), sense.get_temperature(), sense.get_pressure()]
		
		ram_stat = get_ram_stats()
		net_io   = get_network_in_out()
		print net_io
		break
		cpu_temp = get_cpu_temp()
		cpu_load = get_cpu_loadavg()
		
		col = [ram_stat['MemAvailable'], net_io['bytes_rx'], net_io['bytes_tx'], cpu_temp, cpu_load['1'], cpu_load['5'], cpu_load['15']]
		
		col = ','.join([str(x) for x in col])
		print "Sending", col
		sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
		sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		sock.connect(("fcc0:4301:5782:ab81:f533:5b07:c8f3:af89", 8192))
		sock.sendall(col)
		sock.close()
		time.sleep(1)

	sock.shutdown()
	sock.close()

if __name__ == "__main__":
	main()

