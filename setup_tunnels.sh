#!/bin/bash

#sudo ip tuntap add mode tun name kasumi
#sudo ip addr add 10.0.1.1/16 dev kasumi
#sudo ip link set kasumi up

v=$(/sbin/ip -o -6 addr list tun0 | awk '{print $4; exit}')

sudo ip netns add relay
sudo ip netns exec relay ip tuntap add mode tun name relay
sudo ip netns exec relay ip addr add $v dev relay
sudo ip netns exec relay ip link set relay up
