#ifndef KASUMI_HPP
#define KASUMI_HPP

#include <cstdint>
#include <fstream>
#include <cstdio>
#include <ctime>
#include <ifaddrs.h>
#include <cstdarg>

#include "easysocket.hpp"

typedef struct {
	uint8_t  version:4;
	uint8_t  ihl:4;
	uint8_t  dscp:6;
	uint8_t  ecn:2;
	uint16_t total_length;
	uint16_t identification;
	uint8_t  flags:3;
	uint16_t fragment_offset:13;
	uint8_t  ttl;
	uint8_t  protocol;
	uint16_t header_checksum;
	
	IP_Address source_ip;
	IP_Address dest_ip;
} __attribute__((packed)) IPv4_Header;

typedef struct {
	uint32_t version_class_flow;
	uint16_t payload_length;
	uint8_t  next_header;
	uint8_t  hop_limit;
	
	IP_Address source_ip;
	IP_Address dest_ip;
} __attribute__((packed)) IPv6_Header;

#ifdef _IPV6
	typedef IPv6_Header IP_Header;
#else
	typedef IPv4_Header IP_Header;
#endif

typedef struct {
	char        identifier[16];
	uint16_t    command;
	uint16_t    payload_length;
	uint8_t     hop_count;
	IP_Address  source;
	IP_Address  destination;
	IP_Address *hops;
}__attribute__((packed)) KasumiHeader;


typedef struct {
	IP_Address              source;
	IP_Address              dest;
	std::vector<IP_Address> hops;
} IP_Hops;



extern std::vector<IP_Hops> hop_map;
int make_hop_map(std::string filename);

void put_address(IP_Address add, char *buff);
int getHops(IP_Address src, IP_Address dst, std::vector<IP_Address> *out);
int getIPForInterface(std::string iface, IP_Address *ret);

int logging_start(std::string file);
int logging_end();
void log(const char *type, const char *fmt, ...);

#endif
