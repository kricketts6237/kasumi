#include "iface.hpp"

IFace::IFace() {
	this->name        = "";
	this->type        = -1;
	this->peer        = -1;
	this->index       = -1;
	this->link_type   = IFace::LINK_NONE;
	this->slave_type  = IFace::LINK_NONE;
	this->master      = -1;
	this->up          = false;
}

void IFace::typeCheck() {
	if (this->type == ARPHRD_LOOPBACK) {
		this->link_type      = IFace::LINK_LOOPBACK;
		this->link_type_name = "loopback";
	} else if (this->type == ARPHRD_ETHER && this->link_type == IFace::LINK_NONE) {
		// Likely a physical interface
		this->link_type      = IFace::LINK_PHYSICAL;
		this->link_type_name = "physical";
	}
}

IFace::LinkType IFace::getLinkType(std::string lt) {
	std::transform(lt.begin(), lt.end(), lt.begin(), ::tolower);
	
	if (lt == "veth") {
		return IFace::LINK_VETH;
	} else if (lt == "bridge") {
		return IFace::LINK_BRIDGE;
	} else if (lt == "bond") {
		return IFace::LINK_BOND;
	} else if (lt == "dummy") {
		return IFace::LINK_DUMMY;
	} else if (lt == "vlan") {
		return IFace::LINK_VLAN;
	}
	
	return IFace::LINK_UNKNOWN;
}

IFace::LinkType IFace::getLinkType(char *lt) {
	return IFace::getLinkType(std::string(lt));
}
