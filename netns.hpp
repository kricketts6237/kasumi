#ifndef NETNS_HPP
#define NETNS_HPP

#include <string>
#include <vector>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>
#include <sched.h>
#include <unistd.h>
#include <memory>

#include "constants.hpp"

class NetNS {
	public:
		std::string name;
		std::string path;
		int         handle;
		
		NetNS();
		
		int prepare();
		int enter();
		int finish();
};

#endif
