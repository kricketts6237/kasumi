#ifndef NETNSMANAGER_HPP
#define NETNSMANAGER_HPP

#include <memory>
#include "netns.hpp"
#include "debugging.hpp"

typedef std::unique_ptr<NetNS> NetNS_ptr;

class NetNSManager {
	public:
		explicit NetNSManager();
		
		NetNSManager(const NetNSManager&) = delete;
		NetNSManager& operator=(const NetNSManager&) = delete;
		
		~NetNSManager();
		
		const std::string home_ns = "proc_self";
		
		int populate();
		int enter(NetNS *ns);
		int leave();
		
		std::vector<NetNS *>  getNamespaces();
		NetNS                *getNamespaceByName(std::string name);
		NetNS                *getCurrentNamespace();
		
	protected:
		std::vector<NetNS_ptr>  namespaces;
		NetNS                  *current;
};

#endif
