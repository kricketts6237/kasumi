#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <map>
#include <vector>

std::map<uint32_t, std::map<uint32_t, std::vector<uint32_t>>> hop_map;
void make_hop_map() {
	char *buffer = (char *)malloc(0x10000);
	FILE *hops = fopen("hops_list", "r");
	int   size;
	
	fseek(hops, 0, SEEK_END);
	size = ftell(hops);
	fseek(hops, 0, SEEK_SET);
	
	fread(buffer, 1, size, hops);
	
	fclose(hops);
	
	char cmd[8];
	int  octets[4];
	
	int i = 0;
	int a = 0;
	while (i < size) {
		while (std::isspace(buffer[i]))
			i++;
		
		sscanf(buffer + i, "%s %d.%d.%d.%d\n%n", cmd, octets+0, octets+1, octets+2, octets+3, &a);
		
		std::cout << "DO(" << cmd << ")  IP(" << octets[0];
		std::cout << "." << octets[1];
		std::cout << "." << octets[2];
		std::cout << "." << octets[3] << ")";
		std::cout << std::endl;
		
		i += a;
	}
}

int main(void) {
	make_hop_map();
}
