nodes =  {
	"alpha": "fcc0:4301:5782:ab81:f533:5b07:c8f3:af89",
	"beta": "fc28:9a00:ccf6:a51f:9305:9004:a191:c973",
	"delta": "fc25:444e:b3bd:1dc:6dc9:d8f1:3734:a8d8",
	"epsilon": "fcb6:c57d:6f0a:e2e9:e8da:d281:72fc:2e83"
}

node_map = {
	"alpha": {
		"beta": [],
		"delta": ["beta"],
		"epsilon": ["beta", "delta"]
	},
	
	"beta": {
		"alpha": [],
		"delta": [],
		"epsilon": ["delta"]
	},
	
	"delta": {
		"alpha": ["beta"],
		"beta": [],
		"epsilon": []
	},
	
	"epsilon": {
		"alpha": ["delta", "beta"],
		"beta": ["delta"],
		"delta": []
	}
}

def main():
	for src in node_map:
		print "source", nodes[src]
		for dst in node_map[src]:
			print "\tdest", nodes[dst]
			for hop in node_map[src][dst]:
				print "\t\thop", nodes[hop]

if __name__ == "__main__":
	main()
