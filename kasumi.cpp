#include "kasumi.hpp"

std::vector<IP_Hops>  hop_map;
FILE                 *logfile;

int make_hop_map(std::string filename) {
	char *buffer = (char *)malloc(0x10000 + sizeof(KasumiHeader) + sizeof(IP_Address) * 256);
	FILE *hops = fopen(filename.c_str(), "r");
	int   size;
	
	if (hops == nullptr) {
		log("ERROR", "Could not find hops_list\n");
		return 1;
	}
	
	fseek(hops, 0, SEEK_END);
	size = ftell(hops);
	fseek(hops, 0, SEEK_SET);
	
	fread(buffer, 1, size, hops);
	
	fclose(hops);
	
	IP_Hops    listing;
	char       cmd[16];
	bool       should_append = false;
	
	int i = 0;
	int a = 0;
	while (i < size) {
		while (std::isspace(buffer[i]))
			i++;
		
		IP_Address addr;
		
		#ifdef _IPV6
			char addr_buff[64];
			
			sscanf(buffer + i, "%s %n", cmd, &a);
			i += a;
			
			sscanf(buffer + i, "%s\n%n", addr_buff, &a);
			
			inet_pton(AF_INET6, addr_buff, &addr);
			
		#else
		union { uint8_t o[4]; uint32_t d; } conv;
		sscanf(buffer + i, "%s %d.%d.%d.%d\n%n", cmd, conv.o+0, conv.o+1, conv.o+2, conv.o+3, &a);
		addr.s_addr = conv.d;
		#endif
		
		if (!strcmp(cmd, "source")) {
			if (should_append) {
				hop_map.push_back(listing);
				listing.hops.clear();
				should_append = false;
			}
			
			listing.source = addr;
		}
		
		if (!strcmp(cmd, "dest")) {
			if (should_append) {
				hop_map.push_back(listing);
				listing.hops.clear();
				should_append = false;
			}
			
			should_append = true;
			
			listing.dest = addr;
		}
		
		if (!strcmp(cmd, "hop")) {
			listing.hops.push_back(addr);
			should_append = true;
		}
		
		i += a;
	}
	
	hop_map.push_back(listing);
	
	return 0;
}

int logging_start(std::string file) {
	if (file == "stdout") {
		logfile = stdout;
		
		return 0;
	} else {
		logfile = fopen(file.c_str(), "a");
		
		if (logfile == NULL)
			return 1;
		return 0;
	}
}

int logging_end() {
	if (logfile != stdout) {
		fclose(logfile);
	}
	
	return 0;
}

void log(const char *type, std::string text, bool append) {
	if (append)
		fprintf(logfile, "%s", text.c_str());
	else
		fprintf(logfile, "%s[%ld]: %s", type, time(NULL), text.c_str());
	
	fflush(logfile);
}

void log(const char *type, char *text, bool append) {
	fprintf(logfile, "%s[%ld]: %s", type, time(NULL), text);
	fflush(logfile);
}

void log(const char *type, const char *text, bool append) {
	fprintf(logfile, "%s[%ld]: %s", type, time(NULL), text);
	fflush(logfile);
}

void log(const char *type, const char *fmt, ...) {
	fprintf(logfile, "%s[%ld]: ", type, time(NULL));
	
	va_list args;
	va_start(args, fmt);
	vfprintf(logfile, fmt, args);
	va_end(args);
	
	fflush(logfile);
}

void put_address(IP_Address add, char *buff) {
	#ifdef _IPV6
		//char buff[64];
		inet_ntop(AF_INET6, &add, buff, 64);
		//log("INFO", buff, true);
	#else
		//char buff[64];
		inet_ntop(AF_INET, &add, buff, 64);
		//log("INFO", buff, true);
	#endif
}


int getHops(IP_Address src, IP_Address dst, std::vector<IP_Address> *out) {
	bool found_source = false;
	for (unsigned int i = 0; i < hop_map.size(); i++) {
		if (!memcmp((void *)&(hop_map[i].source), (void *)&(src), sizeof(IP_Address))) {
			found_source = true;
			if (!memcmp((void *)&(hop_map[i].dest), (void *)&(dst), sizeof(IP_Address))) {
				if (out != nullptr)
					*out = hop_map[i].hops;
				return 0;
			}
		}
	}
	
	return (found_source) ? 1 : 2;
}

int getIPForInterface(std::string iface, IP_Address *ret) {
	struct ifaddrs *if_req, *ifa;
	Socket_Addr    *addr;
	char            addr_s[64];
	
	memset(addr_s, 0, 64);
	
	getifaddrs(&if_req);
	for (ifa = if_req; ifa; ifa = ifa->ifa_next) {
		if (ifa->ifa_addr == NULL)
			continue;
		#ifdef _IPV6
		if (strcmp(iface.c_str(), ifa->ifa_name))
			continue;
			
		if (ifa->ifa_addr->sa_family == AF_INET6) {
			addr = (Socket_Addr *)ifa->ifa_addr;
			
			*ret = addr->sin6_addr;
			
			return 0;
		}
		#else
		if (ifa->ifa_addr->sa_family == AF_INET) {
			addr = (Socket_Addr *)ifa->ifa_addr;
			
			*ret = addr->sin_addr;
			
			return 0;
		}
		#endif
	}
	
	freeifaddrs(if_req);
	
	return -1;
}
