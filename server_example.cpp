#include <iostream>
#include <signal.h>

#include "easysocket.hpp"
#include "netnsmanager.hpp"

bool done = false;
void sigint_handler(int a) {
	done = true;
}

int main(void) {
	signal(SIGINT, sigint_handler);
	
	EasySocketHandler handler;
	
	char *buffer;
	int   err, sock, conn;
	
	std::vector<int> clients;
	
	conn = 0;
	
	buffer = (char *)malloc(0x10000);
	
	#ifdef _IPV6
	sock = handler.createSocket(AF_INET6, SOCK_STREAM, 0, &err);
	#else
	sock = handler.createSocket(AF_INET, SOCK_STREAM, 0, &err);
	#endif
	
	
	if (sock == -1) {
		std::cout << "Unable to create socket: " << strerror(err) << std::endl;
		free(buffer);
		return 1;
	}
	
	Socket_Addr local;
	memset(&local, 0, sizeof(local));
	#ifdef _IPV6
	local.sin6_family = AF_INET6;
	local.sin6_port   = htons(9000);
	inet_pton(AF_INET6, "::1", local.sin6_addr.s6_addr);
	#else
	local.sin_family = AF_INET;
	local.sin_port   = htons(9000);
	inet_pton(AF_INET, "127.0.0.1", &(local.sin_addr.s_addr));
	#endif
	
	err = handler.bind(sock, &local);
	if (err) {
		std::cout << "Unable to bind socket: " << strerror(err) << std::endl;
		free(buffer);
		return 1;
	}
	
	err = handler.listen(sock, 5);
	if (err) {
		std::cout << "Unable to listen on socket: " << strerror(err) << std::endl;
		free(buffer);
		return 1;
	}
	
	std::cout << "Socket created, bound, and listening" << std::endl;
	
	Socket_Addr remote_addr;
	socklen_t   remote_addr_len;
	while (!done) {
		handler.poll(1000);
		
		if (err == -1) {
			std::cout << "Error in poll()" << std::endl;
		} else if (err == 0) {
			std::cout << "Nothing in poll()" << std::endl;
		}
		
		if (handler.readyToRead(sock)) {
			conn = handler.accept(sock, &remote_addr, &remote_addr_len, &err);
			std::cout << "Something appeared on the socket" << std::endl;
			if (conn == -1) {
				std::cout << "Unable to accept socket " << strerror(err) << std::endl;
				free(buffer);
				return 1;
			} else {
				if (err == 0) {
					std::cout << "Accepted connection!" << std::endl;
					clients.push_back(conn);
				} else {
					conn = 0;
					std::cout << "EWOULDBLOCK" << std::endl;
				}
			}
		}
		
		for (auto it = clients.begin(); it != clients.end(); it++) {
			int cli = *it;
			if (handler.readyToRead(cli)) {
				std::cout << "READ! Connection " << cli << std::endl;
				int got = handler.read(cli, buffer, 0x10000, &err);
				if (got == -1) {
					std::cout << "Read returned " << strerror(err) << std::endl;
					free(buffer);
					return 1;
				}
				
				std::cout << "Read " << got << " bytes" << std::endl;
				
				if (got == 0) {
					std::cout << "Client must have disconnected." << std::endl;
					handler.close(cli);
					
					clients.erase(
						std::remove(clients.begin(), clients.end(), cli), 
						clients.end()
					);
					
					break;
				}
			}
		}
	}
	
	
	free(buffer);
	
	std::cout << "Goodbye" << std::endl;
	
	return 0;
}
