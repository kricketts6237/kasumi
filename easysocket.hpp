#ifndef EASYSOCKET_HPP
#define EASYSOCKET_HPP

#include <vector>
#include <memory>
#include <cstdint>
#include <cstring>
#include <algorithm>

#include <sys/socket.h>
#include <netinet/in.h>
#include <linux/if_tun.h>
#include <arpa/inet.h> 
#include <net/if.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <poll.h>

#include "debugging.hpp"

typedef enum {
	SOCKET, TUNNEL
} EasySocketType;

#ifdef _IPV6
	typedef in6_addr     IP_Address;
	typedef sockaddr_in6 Socket_Addr;
#else
	typedef in_addr      IP_Address;
	typedef sockaddr_in  Socket_Addr;
#endif

class EasySocket {
	friend class EasySocketHandler;
	public:
		EasySocketType type;
		Socket_Addr    addr;
		char           name[IFNAMSIZ];
		int            handle;
		
		struct pollfd getPollFD();
	
	private:
		short r_events;
};

typedef std::unique_ptr<EasySocket> EasySocket_ptr;

class EasySocketHandler {
	public:
		std::vector<EasySocket_ptr> file_descriptors;
		
		EasySocketHandler();
		~EasySocketHandler();
		
		EasySocket *createTunnel(char *tunName, int flags, int *err);
		EasySocket *createSocket(int domain, int type, int protocol, int *err);
		int         bind(EasySocket *sock, Socket_Addr *local);
		int         listen(EasySocket *sock, int bl);
		EasySocket *accept(EasySocket *sock, Socket_Addr *remote, socklen_t *remote_len, int *err);
		int         connect(EasySocket *sock, Socket_Addr *remote);
		int         close(EasySocket *sock);
		void        closeAll();
		int         read(EasySocket *sock, char *buffer, int n, int *err);
		int         read_exactly(EasySocket *sock, char *buffer, int n, int *err);
		int         write(EasySocket *sock, char *buffer, int n, int *err);
		bool        readyToRead(EasySocket *sock);
		bool        exists(EasySocket *sock);
		int         poll();
		int         poll(int ms);
		EasySocket *regSocket(EasySocket *sock);
		void        unregSocket(EasySocket *sock);
};

#endif
