#! /bin/sh

if !(ip netns list | grep relay > /dev/null)
then
	sh setup_tunnels.sh
fi

systemctl start kasumi.service
