#ifndef IFACEMANAGER_HPP
#define IFACEMANAGER_HPP

#include <memory>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <string>
#include <sstream>

#include <iostream>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <net/if.h>

#include "iface.hpp"
#include "constants.hpp"
#include "debugging.hpp"

typedef std::unique_ptr<IFace> IFace_ptr;

class IFaceManager {
	public:
		explicit IFaceManager() = default;
		
		IFaceManager(const IFaceManager&) = delete;
		IFaceManager& operator=(const IFaceManager&) = delete;
		
		~IFaceManager() = default;
		
		int populate();
		int populateAddresses();
		
		std::vector<IFace *> getInterfaces();
		
		bool   containsInterface(IFace *iface);
		IFace *getInterfaceByIndex(int idx);
		
	protected:
		std::vector<IFace_ptr> interfaces;
		
		// Used to clean up the populate function
		int process_LINKINFO(IFace *iface, struct rtattr *rta);
		int process_INFO_DATA_BOND(IFace *iface, struct rtattr *rta);
		int process_INFO_SLAVE_DATA_BRIDGE(IFace *iface, struct rtattr *rta);
		int process_INFO_SLAVE_DATA_BOND(IFace *iface, struct rtattr *rta);
};

#endif
